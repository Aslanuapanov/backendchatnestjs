import { Logger } from '@nestjs/common';
import { MessageBody, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';

@WebSocketGateway(3002)
export class AppGateway {
  @WebSocketServer()
  wss;

  private logger = new Logger('AppGateWay');
  handleConnection(client) {
    this.logger.log('New client connected');
    client.emit('connection', 'Successfully connected')
  }

  handleMessage(@MessageBody() data: any): string {
    return data;
  }
}
