import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from '../shared/user.service';
import { LoginDTO, RegisterDTO } from './auth.dto';
import { AuthService } from './auth.service';
import { Payload } from '../types/payload'
import { User } from 'src/utilities/user.decorator';
import { CreatedGuard } from 'src/guards/created.guard';

@Controller('auth')
export class AuthController {
  constructor(private userService: UserService, private authService: AuthService) {}

  @Get()
  // @UseGuards(AuthGuard('jwt'), CreatedGuard)
  async findAll(@User() user: any) {
    return this.userService.findAll();
  }

  @Post('login')
  async login(@Body() userDTO: LoginDTO) {
    const user = await this.userService.findByLogin(userDTO);
    const payload: Payload = {
      username: user.username,
      creater: false,
    };

    const token = await this.authService.signPayload(payload);
    return { user, token };
  }

  @Post('register')
  async register(@Body() userDTO: RegisterDTO){
    const user = await this.userService.create(userDTO);
    const payload: Payload = {
      username: user.username,
      creater: false,
    }

    const token = await this.authService.signPayload(payload);
    return { user, token }
  }
}
