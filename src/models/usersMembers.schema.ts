import * as mongoose from 'mongoose'

export const MembersSchema = new mongoose.Schema({
  users: Array,
  created: {
    type: Date,
    default: Date.now,
  }
})

// Conversation:
// { id: 123
//   participants: ['john', 'marry'],
// }


// Message:
// { sender: 'john', 
//   content: 'howdy', 
//   time_created: new Date(),
//   converstationId: 123
// },
// { sender: 'marry', 
//   content: 'good u', 
//   time_created: new Date(),
//   converstationId: 123 
// },