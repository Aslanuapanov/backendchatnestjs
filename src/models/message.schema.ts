import * as mongoose from 'mongoose'

export const MessageSchema = new mongoose.Schema({
  username: String,
  message: String,
  chatId: String,
  created: {
    type: Date,
    default: Date.now,
  }
})