import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { AppGateway } from './app.gateway';
import { MessageModule } from './message/message.module';

@Module({
  imports: [MongooseModule.forRoot(process.env.MONGO_URI), SharedModule, AuthModule, MessageModule],
  controllers: [AppController],
  providers: [AppService, AppGateway],
})
export class AppModule {}
