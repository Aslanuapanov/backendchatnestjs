export interface Payload {
  username: string;
  creater: boolean;
  iat?: number;
  expiresIn?: string;
}