import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from "@nestjs/common";

@Injectable()
export class CreatedGuard implements CanActivate {
  constructor(){}

  canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();
    const user = req.user;
    console.log(req.user)

    if (user && user.creater) {
      return true;
    } else {
      throw new HttpException('Unauthorizaed access', HttpStatus.UNAUTHORIZED)
    }
  }
}