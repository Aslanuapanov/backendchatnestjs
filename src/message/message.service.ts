import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AppGateway } from 'src/app.gateway';
import { Message } from 'src/message/message.dto';
import { Members } from 'src/message/usersMembers.dto';

@Injectable()
export class MessageService {
  constructor(
    @InjectModel('Message') 
    private messageModel: Model<Message>,
    @InjectModel('Members')
    private membersModel: Model<Members>,
    private gateway: AppGateway
  ){}

  async createMessage(message: Message) {
    const createMessage = new this.messageModel(message);
    this.gateway.wss.emit('newMessage', createMessage);
      return await createMessage.save();
  }

  async getAllMessage() {
    return await this.messageModel.find();
  }

  async getChatMessage(chatId) {
    return await this.messageModel.find({chatId: {$exists: true, $in: [chatId.chatId]}}).exec()
  }

  async usersMembers(members: Members) {
    const usersMembers = new this.membersModel(members)
    console.log(members);
    
    return await usersMembers.save();
  }
}
