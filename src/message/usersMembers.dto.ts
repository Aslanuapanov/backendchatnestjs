import { Document } from 'mongoose';

export interface Members extends Document {
  users: String[];
  created: Date;
}