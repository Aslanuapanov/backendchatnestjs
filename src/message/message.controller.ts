import { Body, Controller, Get, Post } from '@nestjs/common';
import { Message } from 'src/message/message.dto';
import { Members } from 'src/message/usersMembers.dto';
import { MessageService } from './message.service';

@Controller('message')
export class MessageController {
  constructor(private messageService: MessageService) {}

  @Get()
  async getAllMessage() {
    return await this.messageService.getAllMessage()
  }

  @Post('usersMessage')
  async getChatMessage(@Body() chatId: string) {
    return await this.messageService.getChatMessage(chatId)
  }

  @Post('usersMembers')
  async usersMembers(@Body() members: Members) {
    return await this.messageService.usersMembers(members);
  }

  @Post('createmessage')
  async createmessage(@Body() message: Message) {
    return await this.messageService.createMessage(message);
  }
}
