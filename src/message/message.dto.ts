import { Document } from 'mongoose';

export interface Message extends Document {
  username: String;
  message: String;
  created: Date;
  chatId: String;
}