import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import { AppGateway } from 'src/app.gateway';
import { MongooseModule } from '@nestjs/mongoose';
import { MessageSchema } from 'src/models/message.schema';
import { MembersSchema } from 'src/models/usersMembers.schema';

@Module({
  imports: [MongooseModule.forFeature([
    {
      name: 'Message',
      schema: MessageSchema,
    },
    {
      name: 'Members',
      schema: MembersSchema,
    }
  ])],
  providers: [MessageService, AppGateway],
  controllers: [MessageController]
})
export class MessageModule {}
