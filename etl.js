(async () => {
  try {
  const { data } = await axios.post('http://localhost:3000/auth/login', {
    username: 'aslanuapanov',
    password: 'password',
  });
  const { token } = data;
  let config = {
    headers: { Authorization: `Bearer ${token}` }
  }
  axios
    .get('http://localhost:3000/message', config)
    .then(res => res.data.map(item => {
      document.getElementById('app').innerHTML += ` 
        <li>
          <div>
            <h2>${item.username}</h2>
            <p>${item.message}</p>
          </div>
        </li>`
    }))
    const socket = io('http://localhost:3001');
  socket.on('connection', (data) => {
    console.log(data);
  });
  socket.on('newMessage', (data) => {
    console.log(data);
    document.getElementById('app').innerHTML += `
      <li>
        <div>
          <h2>${data.username}</h2>
          <p>${data.message}</p>
        </div>
      </li>
    `
  })
  } catch (err) {
    console.log(err);
  }
})();


